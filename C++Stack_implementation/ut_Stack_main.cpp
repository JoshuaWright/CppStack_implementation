/*==========================================
	ut_Stack_main.cpp
	author: Joshua Wright
	contact: joshuawright1197@gmail.com
	Date: 2016-12-02
	description : Stack Unit test
============================================*/

#define BOOST_TEST_MODULE StackTest
#define FULL_TEST true

#include <boost\test\unit_test.hpp>
#include <iostream>
#include <functional>
#include "Stack.hpp"
#include <string>
using namespace std;


BOOST_AUTO_TEST_CASE(intro) {
	cout << "\nStack Unit Test\n";
	cout << "Last compiled: " << __TIMESTAMP__ << "\n\n";
}

BOOST_AUTO_TEST_CASE(Stack_constructors_test) {
	Stack<int> s;
	BOOST_CHECK(s.size() == 0);
	BOOST_CHECK(s.empty() == true);

	s.push(45);
	s.push(65);
	s.push(76);

	Stack<int> sCopy(s);
	BOOST_CHECK(sCopy.size() == s.size());
	BOOST_CHECK(sCopy.top() == s.top());

	Stack<int> sMove(std::move(s));
	BOOST_CHECK(sMove.size() == sCopy.size());
	BOOST_CHECK(sCopy.top() == sMove.top());

	sMove.pop();
	sCopy.pop();
	BOOST_CHECK(sCopy.top() == sMove.top());

	BOOST_CHECK(s.size() == 0);

	BOOST_CHECK(sCopy.empty() == false);
}

BOOST_AUTO_TEST_CASE(Stack_operator_test) {
	Stack<string> s;
	s.push("stack");
	s.push("c++");
	s.push("string");

	Stack<string> sCpyOp;
	sCpyOp = s;
	BOOST_CHECK(sCpyOp.size() == s.size());
	BOOST_CHECK(sCpyOp.top() == s.top());

	sCpyOp.pop();
	s.pop();
	BOOST_CHECK(sCpyOp.size() == s.size());
	BOOST_CHECK(sCpyOp.top() == s.top());

	Stack<string> sMoveOp;
	sMoveOp = std::move(s);
	BOOST_CHECK(sCpyOp.size() == sMoveOp.size());
	BOOST_CHECK(sCpyOp.top() == sMoveOp.top());
	BOOST_CHECK(s.size() == 0);
}

BOOST_AUTO_TEST_CASE(Stack_push_clear_test) {
	Stack<int> s;
	s.push(4);
	BOOST_CHECK(s.size() == 1);
	BOOST_CHECK(s.top() == 4);

	s.push(16);
	BOOST_CHECK(s.size() == 2);
	BOOST_CHECK(s.top() == 16);

	s.clear();
	BOOST_CHECK(s.size() == 0);
	BOOST_CHECK(s.empty() == true);
}

BOOST_AUTO_TEST_CASE(Stack_pop_test) {
	Stack<int> s;
	s.push(44);
	BOOST_CHECK(s.pop() == 44);
	BOOST_CHECK(s.size() == 0);

	s.push(65);
	s.push(32);
	BOOST_CHECK(s.pop() == 32);
	BOOST_CHECK(s.size() == 1);
	BOOST_CHECK(s.top() == 65);
}

#if FULL_TEST
BOOST_AUTO_TEST_CASE(Stack_full_test) {
	Stack<int> s;
	int value;
	function<int(size_t)> make_value = [](size_t index)->int {
		return index * 10;
	};

	for (size_t i = 1; i <= 15000; ++i) {
		value = make_value(i);
		s.push(value);
		BOOST_CHECK(s.top() == value);
	}
	for (size_t i = s.size(); i != 0; --i) {
		BOOST_CHECK(s.size() == i);
		BOOST_CHECK(s.pop() == make_value(i));
	}
}
#endif 