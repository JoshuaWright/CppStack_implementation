/*==========================================
	Stack.hpp
	author: Joshua Wright
	contact: joshuawright1197@gmail.com
	Date: 2016-12-02
	description : Stack implementation 
============================================*/


#pragma once
#include <memory>


/* templated Stack class */
template<class T>
class Stack
{
public:
	using value_type			= T;
	using value_pointer_type	= T*;
	using value_reference_type	= T&;

private:	

	struct node {
		value_type				value_;
		std::shared_ptr<node>	bottomNode_;
	};

	std::shared_ptr<node>  value_;
	size_t					size_;

public:
	Stack() : size_(0), value_(nullptr) {}

	Stack(Stack const&)				= default;
	Stack& operator=(Stack const&) 	= default;

	Stack(Stack&& stk): size_(stk.size_),value_(stk.value_) {
		stk.size_ = 0;
		stk.value_ = nullptr;
	}
	Stack& operator=(Stack&& stk) {
		size_ = stk.size_;
		value_ = stk.value_;
		stk.size_ = 0;
		stk.value_ = nullptr;
		return *this;
	}

	~Stack() {
		clear();
	}

	value_type top() { return value_->value_; }
	size_t size() { return size_; }
	bool empty() { return size_ == 0; }

	void push(const value_type& value) {
		std::shared_ptr<node> add(new node);
		add->value_ = value;
		add->bottomNode_  = value_;
		value_ = add;
		size_++;
	}

	value_type pop() {
		size_--;
		value_type result = value_->value_;	
		value_ = value_->bottomNode_;
		return result;
	}

	void clear() {
		while (!empty()) 
			pop();
	}
};